import React, { useState } from 'react';
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';

const ConfigurationModal = ({ isOpen, onClose }) => {
  const [cashiersCount, setCashiersCount] = useState(0);
  const [chefsCount, setChefsCount] = useState(0);
  const [pizzaTypesCount, setPizzaTypesCount] = useState(0);
  const [minPizzaTime, setMinPizzaTime] = useState(0);
  const [clientGenerationStrategy, setClientGenerationStrategy] = useState('strategy-1'); // Додано стан для стратегії генерації клієнтів

  const handleSave = () => {
    console.log('Cashiers Count:', cashiersCount);
    console.log('Chefs Count:', chefsCount);
    console.log('Pizza Types Count:', pizzaTypesCount);
    console.log('Min Pizza Time:', minPizzaTime);
    console.log('Client Generation Strategy:', clientGenerationStrategy);

    onClose();
  };

  return (
    <Box
      sx={{
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 500,  
        height: 400, 
        background: '#f09e3f',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
        display: 'flex',
        flexDirection: 'column',
        borderRadius: 5,
      }}
    >
      <TextField
        label="Кількість касирів"
        type="number"
        value={cashiersCount}
        onChange={(e) => setCashiersCount(e.target.value)}
        fullWidth
        sx={{ mb: 2 }}
      />
      <TextField
        label="Кількість кухарів"
        type="number"
        value={chefsCount}
        onChange={(e) => setChefsCount(e.target.value)}
        fullWidth
        sx={{ mb: 2 }}
      />
      <TextField
        label="Кількість різних піц в меню"
        type="number"
        value={pizzaTypesCount}
        onChange={(e) => setPizzaTypesCount(e.target.value)}
        fullWidth
        sx={{ mb: 2 }}
      />
      <TextField
        label="Мінімальний час для створення піци (у хвилинах)"
        type="number"
        value={minPizzaTime}
        onChange={(e) => setMinPizzaTime(e.target.value)}
        fullWidth
        sx={{ mb: 2 }}
      />

      <FormControl fullWidth sx={{ mb: 2 }}>
        <InputLabel id="client-generation-strategy-label">Стратегія генерації клієнтів</InputLabel>
        <Select
          labelId="client-generation-strategy-label"
          id="client-generation-strategy"
          value={clientGenerationStrategy}
          label="Стратегія генерації клієнтів"
          onChange={(e) => setClientGenerationStrategy(e.target.value)}
        >
          <MenuItem value="strategy-1">Стратегія 1</MenuItem>
          <MenuItem value="strategy-2">Стратегія 2</MenuItem>
          <MenuItem value="strategy-3">Стратегія 3</MenuItem>
        </Select>
      </FormControl>

      <Button
        variant="contained"
        onClick={handleSave}
        sx={{
          backgroundColor: '#c46414',
          color: '#000',
          '&:hover': {
            backgroundColor: '#a95a1b',
          },
        }}
      >
        Зберегти конфігурацію
      </Button>
    </Box>
  );
};

export default ConfigurationModal;
