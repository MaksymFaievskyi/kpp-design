import React, { useState, useEffect } from 'react';
import './App.css';
import ConfigurationModal from './components/ConfigurationModal';
import image from "./assets/Pizzeria_background.png";
import guest1 from "./assets/character-icons/guest1-icon.png"; 
import guest2 from "./assets/character-icons/guest2-icon.png"; 
import guest3 from "./assets/character-icons/guest3-icon.png"; 
import guest4 from "./assets/character-icons/guest4-icon.png";

import chef from "./assets/character-icons/chef-icon.png"; 

import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';

function App() {
  const [isModalOpen, setModalOpen] = useState(false);

  const handleOpenModal = () => {
    setModalOpen(true);
  };

  const handleCloseModal = () => {
    setModalOpen(false);
  };

  // Викликаємо функцію для відкриття модального вікна при завантаженні сторінки
  useEffect(() => {
    handleOpenModal();
  }, []); // Порожній масив означає, що ефект викликається тільки при монтажі компонента

  return (
    <div className="App">
      <img id="background-photo" src={image} alt="background" />

      {/* Фото покупців */}
        <img className="people-photo" src={guest1} alt="guest1" style={{ top: '100px', left: '200px' }} />
        <img className="people-photo" src={guest2} alt="guest2" style={{ top: '300px', left: '400px' }} />
        <img className="people-photo" src={guest3} alt="guest3" style={{ top: '500px', left: '600px' }} />
        <img className="people-photo" src={guest4} alt="guest4" style={{ top: '500px', left: '900px' }} />
      {/* Фото кухарів */}
        <img className="people-photo" src={chef} alt="chef" style={{ top: '700px', left: '300px' }} />
        <img className="people-photo" src={chef} alt="chef" style={{ top: '700px', left: '600px' }} />

      
      <Modal
        open={isModalOpen}
        onClose={handleCloseModal}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box
          sx={{
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            width: 400,
            background: 'rgba(0, 0, 0, 0.5)',  // Затемнений фон з прозорістю
            p: 4,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <ConfigurationModal onClose={handleCloseModal} />
        </Box>
      </Modal>
    </div>
  );
}

export default App;